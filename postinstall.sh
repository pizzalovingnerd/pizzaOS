wget https://gitlab.com/pizzalovingnerd/pizzaOS/raw/master/panel.ini
cat ~/panel.ini | dconf load /com/solus-project/budgie-panel/
rm ~/panel.ini
printf "We are about to install the AUR, and extra applications from the AUR. If you don't want to AUR, close this window (and reboot) or hit CTRL-C (and reboot).\nIt is recommended to install the AUR because the gtk theme (Plata) and Software Center (Pamac) are from the AUR.\n\n"
read -p "Press enter to continue"
git clone https://aur.archlinux.org/yay.git
cd yay/
makepkg -si --noconfirm
cd ..
rm -rf yay
yay -S --noconfirm pamac-aur google-earth micro snapd plata-theme vscodium-bin simplenote discord
gsettings set org.gnome.desktop.interface gtk-theme "Plata-Noir-Compact"
gsettings set org.gnome.desktop.interface icon-theme 'Papirus-Dark'
gsettings set org.gnome.desktop.wm.preferences theme "Plata-Noir-Compact"
read -p "We need to reboot, press enter to reboot."
sudo rm -rf ~/postinstall.sh
sudo reboot