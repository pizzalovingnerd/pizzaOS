#!/usr/bin/env bash

akversion="2.0"

if [ "$1" = "inchroot" ]
then
    echo ">>> If you see this text, you are now in your system (or chrooted)!"
    sleep 1
    echo ">>> Getting timezones ready..."
    while true
    do
	clear
	echo ">>> Listing all Zones"
	ls /usr/share/zoneinfo/
	echo -n "Please type your zone [EX America]: "
	# dia
	read zonein
	if [ ! -d "/usr/share/zoneinfo/$zonein" ]
	then
		echo "ERROR: Please enter a valid zone"
		echo "Make sure you include the capital letters!"
		sleep 2
	else
		break
	fi
    done

    while true
    do
	clear
	echo "Listing all Zones"
	ls "/usr/share/zoneinfo/$zonein"
	echo -n "Please type your Subzone [EX New_York]: "
	read subin
	if [ ! -f "/usr/share/zoneinfo/$zonein/$subin" ]
	then
	    echo -n "ERROR: Please enter a valid subzone\nMake sure you include the capital letters" 10 40
	    sleep 2
	else
	    break
	fi
    done
    ln -sf /usr/share/zoneinfo/$zonein/$subin /etc/localtime
    echo ">>> Using hardware clock to generate..."
    hwclock --systohc
    clear
	echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
    echo ">>> Generating the locale!"
    locale-gen
	echo LANG=en_US.UTF-8 > /etc/locale.conf
    export LANG=en_US.UTF-8
    clear
    echo "Note, use A-Z,'-' characters only."
    echo "Example hostname: archlinux-pc"
    echo ""
    echo -n "Please type your system hostname: "
    read hostnm
    echo "$hostnm" >> /etc/hostname
    echo ">>> Installing wifi drivers... [if you use ethernet it wont matter]"
    pacman -Syu dialog wpa_supplicant iw
    clear
    echo ">>> Setting root password"
    passwd
    echo ">>> Installing grub"
    pacman -Syu --noconfirm grub
    echo -n "Please enter hard drive to install grub on (not including /dev/) [ex: sda, sdb, sdc]: "
    read grubpartname
    echo ">>> Installing grub"
    grub-install --target=i386-pc /dev/$grubpartname
    echo ">>> Generating grub file..."
    grub-mkconfig -o /boot/grub/grub.cfg
    clear
    echo ">>> Create your account "
    echo "Please use characters a-z, A-Z, 1-9, and '-'."
    echo "Good username: bobthe-Cat0123"
    echo "BAD username: Bobby The Cat 42"
    echo ""
    echo -n "Please enter your username: "
    read usname
    useradd -m -s /bin/bash $usname
    passwd $usname
    echo ">>> User $usname has been created!"
    echo "$usname ALL=(ALL) ALL" >> /etc/sudoers
    echo ">>> User creation done..."
    sed -i "/\[multilib\]/,/Include/"'s/^#//' /etc/pacman.conf
	echo "Installing Stuff..."
	echo "Continue pressing enter to accept all dependencies"
    read -p Press enter to continue
    pacman -Syu --noconfirm xorg xorg-server networkmanager network-manager-applet budgie-desktop sddm cheese evince gdm gedit gnome-control-center gnome-backgrounds gnome-screensaver gnome-books gnome-calculator gnome-calendar gnome-characters gnome-clocks gnome-color-manager gnome-contacts gnome-dictionary gnome-documents gnome-font-viewer gnome-keyring gnome-logs gnome-maps gnome-music gnome-photos gnome-remote-desktop gnome-screenshot gnome-settings-daemon tilix gnome-todo gnome-video-effects gnome-weather simple-scan dconf-editor gnome-chess gnome-recipes gnome-nettool gnome-usage polari quadrapassel sysprof adapta-gtk-theme papirus-icon-theme chromium firefox lutris geany kdenlive audacity gimp gparted geary handbrake mypaint lmms krita obs-studio vlc libreoffice-fresh git wget yajl steam steam-native-runtime flatpak gparted telegram-desktop
	echo "export XDG_CURRENT_DESKTOP=Budgie:GNOME" > /home/$usname/.xinitrc
	echo "exec budgie-desktop" > /home/$usname/.xinitrc
	systemctl enable sddm.service
    systemctl enable NetworkManager
    systemctl enable pulseaudio.socket
    echo -n "We are about to install WINE now. If you do not wine, then press n and enter when it asks to \"proceed with installation?\""
    echo -n " "
    read -p "Press enter to continue"
    pacman -S wine wine-mono wine_gecko winetricks playonlinux
    clear
    echo -n ">>> Getting post install file..."
    wget https://gitlab.com/pizzalovingnerd/pizzaOS/raw/master/postinstall.sh
    chmod 777 /home/$usname/postinstall.sh
    su $usname -c bash postinstall.sh
    echo -n ">>> Removing Archkick..."
    rm -rf /usr/bin/archkick
    echo "Hooray! :D"
    sleep 1
    echo "Your setup is done!"
    sleep 1
    echo "You have successfully installed pizzaOS onto your system!"
    sleep 1
    echo ""
    read -p "Press enter to reboot"
    exit
else
    echo "Welcome to the PizzaOS installer. This installer uses bits of code from the Arch Wiki, and archkick."
    echo -n " "
    read -p "Press enter to continue"
    echo -n ">>> Updating system clock"
    timedatectl set-ntp true
    clear
    echo ">>> Partitioning "
    echo "Showing all devices"
    lsblk
    echo "Please format your answer like this, sda, sdb, sdc, and so on..."
    echo "DO NOT INCLUDE THE /dev/ PART IN IT!"
    echo ""
    echo -n "Please enter your host system to install on (Not including /dev/) [ex: sda, sdb, sdc]: "
    read partname
    echo -n "What number for /dev/$partname are you putting root on? [ex: 1, 2, 3]: "
    read partnum
    while true
    do
	echo "Are you going to use swap? [y/n]"
	read -rsn1 swapa
	if [ "$swapa" = "y" ]
	then
	    echo -n "What number for /dev/$partname are you putting SWAP on? [ex: 1, 2, 3]: "
	    read swapnum
	    swap="on"
	    break
	elif [ "$swapa" = "n" ]
	then
	    echo ">>> Swap will NOT be selected"
	    swap="off"
	    break
	else
	    clear
	fi
    done
    dialog --backtitle "ArchKick v$akversion" --msgbox "Opening cfdisk for ease of use. If you are unsure how to use it, you can always pull up a second device and see how to use it. Remember to write the changes to disk once done.\n\nSummary\n- Root will be on $partname$partnum\n- Swap will be on $partname$swapnum" 13 50
    cfdisk /dev/$partname
    echo "Formatting root as ext4..."
    mkfs.ext4 /dev/$partname$partnum
    if [ "$swap" = "on" ]
    then
	echo ">>> Creating swap..."
	mkswap /dev/$partname$swapnum
	swapon /dev/$partname$swapnum
    fi
    mount /dev/$partname$partnum /mnt
    dialog --backtitle "ArchKick v$akversion" --yesno "Do you want to edit the mirrorlist file? You probably should." 7 50
    rchc=$?
    if [ $rchc = 0 ]
    then
	nano /etc/pacman.d/mirrorlist
    elif [ $rchc = 0 ]
    then
	echo ">>> Skipping then..."
    else
	echo ">>> Skipping then..."
    fi
    clear
	echo ">>> Installing the base and base-devel!"
	pacstrap /mnt base base-devel
    genfstab -U /mnt >> /mnt/etc/fstab
    chmod +x archkick.sh
    cp archkick.sh /mnt/usr/bin/archkick
    arch-chroot /mnt archkick inchroot $partname
    reboot
fi
