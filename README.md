# pizzaOS - an Arch based OS that's actually good.

Manjaro, and Antargos are horrible (In my opinion). They are buggy, unstable, and break easily. pizzaOS isn't technically a distro. It's just Arch with a custom install script. No custom repos, no bloat.

pizzaOS runs Budgie, and it comes with every application I actually use. While it could be considered bloated by some, in my opinion it's the perfect number of applications.

# Please note

- pizzaOS isn't finished yet.
- pizzaOS is not for the new user
- pizzaOS at the moment only supports en_US.
- pizzaOS only works on Legacy BIOS.
- pizzaOS only works on Hard Drives with the DOS partion table. If you hard drive uses GPT, you can use the [gparted](https://gparted.org/download.php) iso to change it to DOS

# Change Locales

If you don't use en_US, replace en_US with your locales on line 47, 50, and 51

# Known Issues

- No Desktop Icons. (Nautilus 3.28 Doesn't support desktop icons. To fix this we would need to install Nemo, however this installs the whole Cinnamon DE.)
- Characters from other languages and emojis don't display currectly.

# Running ArchKick from a fresh Arch USB

pizzaOS uses a fork of ArchKick (created by SadError256) to install it.

Go ahead and download an Arch Linux ISO from the [download page](https://www.archlinux.org/download/), put it on a usb and boot it up. If you use wifi connect to it with this command.

    wifi-menu

And it should give you a menu to connect to a network. If you use Ethernet, it should work out of the box!

Now go ahead and type out this command on the bash shell. If you are installing on the same machine take a picture or write it down!

    wget https://gitlab.com/pizzalovingnerd/pizzaOS/raw/master/archkick.sh && bash archkick.sh

It is a little long for a command, but it will download and run the script! Once its done, well, you should see a welcome screen, if so, great, you are running ArchKick!

Once you are done, reboot. After you reboot, login, open a terminal window, and type

    bash postinstall.sh
    
Note:
For whatever reason, the postinstall.sh has nothing in it. If you have this issue, run the following command to run the postinstall

    rm postinstall.sh && wget https://gitlab.com/pizzalovingnerd/pizzaOS/raw/master/postinstall.sh && bash postinstall.sh
    
After that it will theme pizzaOS, and ask you if you want to install the AUR, and AUR applications

# What next?
Follow the instructions. Make sure to be at the computer, its not just a type type and you can walk away, it will ask some questions on the go.

# Applications
pizzaOS comes with the following list of applications:

- Audacity
- Brasero
- Budgie Desktop (The Default Desktop)
- Cheese
- Chromium
- Dconf Editor
- Devhelp
- emacs
- Eye of GNOME
- Firefox
- Geary
- Geany
- Gedit
- GNOME Books
- GNOME Boxes
- GNOME Calculator
- GNOME Calendar
- GNOME Chess
- GNOME Contacts
- GNOME Control Center
- GNOME Documents
- GNOME Desktop (You can use GNOME by switching to it in LightDM)
- GNOME Dictionary
- GNOME Mahjongg
- GNOME Mines
- GNOME Nibbles
- GNOME Photos
- GNOME Screenshot
- GNOME Software
- GNOME Sudoku
- GNOME System Monitor
- GNOME Terminal
- GNOME Usage
- GNOME Weather
- GIMP
- Google Earth (Only if you install the AUR)
- Gparted
- Handbrake
- Kdenlive
- Krita
- LMMS
- Lutris
- Mypaint
- Micro (Awesome Terminal Text Editor) (Only if you install the AUR)
- Nautilus
- OBS
- Pamac (Only if you install the AUR)
- Papirus Icon Theme
- Plata GTK Theme
- PlayOnLinux (Only if you choose to install WINE)
- Polari
- PuTTY
- Quadrapassel
- Simplenote (Only if you install the AUR)
- Vim
- VLC
- Visual Studio Codium (Only if you install the AUR)
- Wine (Optional)
- Winetricks (Only if you choose to install WINE)
- Yaourt (Only if you install the AUR)

# Customize pizzaOS

Once you run the postinstall script, it will customize it to my likings. However, we all have different preferences, so here are some tips for customizing pizzaOS

### Switch to GNOME

If you don't like Budgie, GNOME also comes preinstalled. To switch to it, logout. Once you've logged out, on the top right, you should see the budgie logo. Just click on that and click GNOME. I recommend clicking on "GNOME on Xorg" because Wayland sucks.

### Desktop Backgrounds

If you have ran the postinstall script, you will have a bunch of pictures in your pictures folder. You can use these as desktop backgrounds. To switch your desktop background, open your settings app, navigate to background, then "Background". Once you've got here, it will list all your background choices. To use a GNOME background, go to the Wallpapres tab. If you want to use a pizzaOS wallpaper, go to the pictures tab and choose your background, or navigate to the pictures folder, right click the image, and set it as your wallpaper. If you want to use a certain color, navigate to the colors tab. If you would like to use your own image, either right click it, and click "set as wallpaper", or put it in your Pictures folder, and choose it from the pictures tab in the wallpaper settings.

### Customizing Budgie

If you would like to learn how to customize Budgie, here is an article on how to customize it:

https://www.addictivetips.com/ubuntu-linux-tips/customize-the-budgie-desktop/

### Customizing GNOME

If you would like to learn how to customize GNOME, here are some articles on how to customize it:

https://www.zdnet.com/article/how-to-customise-your-linux-desktop-gnome-3/
https://www.ubuntupit.com/customize-gnome-shell-tips-beautify-gnome-desktop/

# Make your own OS (Outdated):

## Archkick.sh

Fork this project, and go open the archkick.sh file. Now go to line 90, and edit the pacman -S file to add and remove packages. However there are some important stuff you need to keep

You need to keep ```xorg xorg-server networkmanager network-manager-applet lxdm-gtk3```. You also need to add a DE.

Notes: 
- If you use a QT DE like plasma, replace ```network-manager-applet``` with ```networkmanager-qt```
- If you are on GNOME, replace ```sddm``` with ```gdm``` on both line 90 and 92
- You can also use LightDM instead of sddm, however is has a issue where the background is always black

After that add as many applications to the line as you want. If there's a package group like ```gnome``` or ```gnome-extra```, that installs packages you don't want, you can add them to line 91 to remove it.

If you don't want the opinion for wine, remove line 95-98. You can also edit the line to remove PlayOnLinux

## Postinstall.sh

Now, your going to want to open postinstall.sh, remove lines 1-4 to remove automatic theming, or change them to your likings, and remove lines 8, 9, and 10 to get rid of pizzaOS wallpapers.

If you want to have pizzaOS install an AUR package, add or remove packages on line 23

# License
Archkick and pizzaOS is licensed under the LGPLv3 license, you are free to modify and fork in your own or public use, just make sure to read the license fully!
